const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        res.data = AuthService.login(req.body);
    } catch (err) {
        res.err = {
            status: 400,
            data: {
                error: true,
                message: 'Login failed'
            }
        }
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
