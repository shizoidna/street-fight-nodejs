const {FighterRepository} = require('../repositories/fighterRepository');

class FighterService {

  getAll() {
    return FighterRepository.getAll();
  }

  create(user) {
    return FighterRepository.create(user);
  }

  update(id, fighter) {
    return FighterRepository.update(id, fighter);
  }


  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  delete(id) {
    return FighterRepository.delete(id);
  }

}

module.exports = new FighterService();
