const { UserRepository } = require('../repositories/userRepository');

class UserService {

    getAll() {
        return UserRepository.getAll();
    }

    create(user) {
        return UserRepository.create(user);
    }

    update(id, user) {
        return UserRepository.update(id, user);
    }


    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        return UserRepository.delete(id);
    }
}

module.exports = new UserService();
