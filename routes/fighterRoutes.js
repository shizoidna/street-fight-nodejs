const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try {
    res.data = FighterService.getAll();
  } catch (err) {
    res.err = {
      status: 400,
      data: {
        error: true,
        message: err
      }
    }
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', isFoundMiddleware, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  try {
    res.data = FighterService.create(req.body);
  } catch (err) {
    res.err = {
      status: 400,
      data: {
        error: true,
        message: err
      }
    }
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', isFoundMiddleware, updateFighterValid, (req, res, next) => {
  try {
    res.data = FighterService.update(req.params.id, req.body);
  } catch (err) {
    res.err = {
      status: 400,
      data: {
        error: true,
        message: err
      }
    }
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', isFoundMiddleware, (req, res, next) => {
  try {
    res.data = FighterService.delete(req.params.id);
  } catch (err) {
    res.err = {
      status: 400,
      data: {
        error: true,
        message: err
      }
    }
  } finally {
    next();
  }
}, responseMiddleware);

function isFoundMiddleware(req, res, next) {
  try {
    res.data = FighterService.search({id: req.params.id});

    if (res.data === null) {
      res.err = {
        status: 404,
        data: {
          error: true,
          message: `User with such ID: ${req.params.id} does not exist.`
        }
      }
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}


module.exports = router;
