const responseMiddleware = (req, res) => {
  if (res.err) {
    res.status(res.err.status).json(res.err.data);
    console.error(res.err.data);
  } else {
    res.json(res.data);
  }

}

exports.responseMiddleware = responseMiddleware;
