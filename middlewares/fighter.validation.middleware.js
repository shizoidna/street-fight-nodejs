const {fighter} = require('../models/fighter');

const createFighterValid = (req, res, next) => {
  fighterValid(req, res, next);
}

const updateFighterValid = (req, res, next) => {
  fighterValid(req, res, next);
}

function fighterValid(req, res, next) {
  let inputFighter = req.body;
  req.body = cropFighter(inputFighter);
  let errorMessage = [validateRequiredFields, validatePower, validateNoId, validateDefense]
    .reduce((acc, element) => {
      if (acc === null) {
        acc = element(inputFighter);
      }

      return acc;
    }, null)

  if (errorMessage !== null) {
    return res.status(400).json({
      error: true,
      message: errorMessage
    });
  }

  next();
}

function validateRequiredFields(inputFighter) {
  let result = null;
  const requiredFields = Object.keys(fighter).filter((element) => element !== 'id' && element !== 'health');
  requiredFields.forEach((element) => {
    if (inputFighter[element] === undefined) {
      result = `Required field ${element} is empty`;
    }
  })

  return result;
}

function validatePower(inputFighter) {
  let result = null;

  if (typeof inputFighter.power !== "number" || inputFighter.power > 100 || inputFighter.power < 1) {
    result = 'Invalid type of power';
  }

  return result;
}

function validateNoId(inputFighter) {
  let result = null;

  if (inputFighter.id) {
    result = 'Invalid params (id).';
  }

  return result;
}

function validateDefense(inputFighter) {
  let result = null;

  if (inputFighter.defense >= 10  || inputFighter.defense <= 1 ) {
    result = 'Invalid value of defense';
  }

  if (typeof inputFighter.defense !== "number") {
    result = 'Invalid type of defense';
  }

  return result;
}

function cropFighter({name, power, defense}) {
  return {
    name,
    power,
    health: fighter.health,
    defense}
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
