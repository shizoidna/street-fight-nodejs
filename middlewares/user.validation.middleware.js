const {user} = require('../models/user');
const createUserValid = (req, res, next) => {
  userValid(req, res, next);
}

const updateUserValid = (req, res, next) => {
  userValid(req, res, next);
}

function userValid(req, res, next) {
  let inputUser = req.body;
  req.body = cropUser(inputUser);
  let errorMessage = [validateRequiredFields, validateEmail, validateTelephone, validatePassword, validateNoId]
    .reduce((acc, element) => {
      if (acc === null) {
        acc = element(inputUser);
      }

      return acc;
    }, null)

  if (errorMessage !== null) {
    return res.status(400).json({
      error: true,
      message: errorMessage
    });
  }
  next();
}


function validateRequiredFields(inputUser) {
  let result = null;
  const requiredFields = Object.keys(user).filter((element) => element !== 'id');
  requiredFields.forEach((element) => {
    if (inputUser[element] === undefined) {
      result = `Required field ${element} is empty`;
    }
  })

  return result;
}

function validateEmail(inputUser) {
  const isMatch = /^[\w-.+]*@gmail\.com/.test(inputUser.email);
  let result = null;

  if (!isMatch) {
    result = 'Invalid user email';
  }

  return result;
}

function validateNoId(inputUser) {
  let result = null;

  if (inputUser.id) {
    result = 'Invalid params: id.';
  }

  return result;
}

function validateTelephone(inputUser) {
  const isMatch = /\+380[0-9]{9}/.test(inputUser.phoneNumber);
  let result = null;

  if (!isMatch) {
    result = 'Invalid user phone number';
  }

  return result;
}

function cropUser({firstName, lastName, email, phoneNumber, password}) {
  return {
    firstName,
    lastName,
    email,
    phoneNumber,
    password
  }
}

function validatePassword(inputUser) {
  let password = inputUser.password;
  let result = null;

  if (password.length < 3) {
    result = 'Invalid user password length';
  }

  return result;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
