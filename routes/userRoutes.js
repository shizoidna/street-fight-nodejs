const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try {
    res.data = UserService.getAll();
  } catch (err) {
    res.err = {
      status: 400,
      data: {
        error: true,
        message: err
      }
    }
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', isFoundMiddleware, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  console.log('post users');
  try {
    res.data = UserService.create(req.body);
  } catch (err) {
    res.err = {
      status: 400,
      data: {
        error: true,
        message: err
      }
    }
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', isFoundMiddleware, updateUserValid, (req, res, next) => {
  try {
    res.data = UserService.update(req.params.id, req.body);
  } catch (err) {
    res.err = {
      status: 400,
      data: {
        error: true,
        message: err
      }
    }
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', isFoundMiddleware, (req, res, next) => {
  try {
    res.data = UserService.delete(req.params.id);
  } catch (err) {
    res.err = {
      status: 400,
      data: {
        error: true,
        message: err
      }
    }
  } finally {
    next();
  }
}, responseMiddleware);

function isFoundMiddleware(req, res, next) {
  try {
    res.data = UserService.search({id: req.params.id});

    if (res.data === null) {
      res.err = {
        status: 404,
        data: {
          error: true,
          message: `User with such ID: ${req.params.id} does not exist.`
        }
      }
    }
  } catch (err) {
    res.err = {
      status: 400,
      data: {
        error: true,
        message: err
      }
    };
  } finally {
    next();
  }
}

module.exports = router;
